﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNet
{
    public static class RandomUtil
    {
        private static Random random = new Random();

        public static float RandomFloat(float min, float max)
        {
            return (float) (RandomFloat()*(max - min) + min);
        }

        public static float RandomClamped()
        {
            return RandomFloat(-1, 1);
        }

        public static float RandomFloat()
        {
            return (float)random.NextDouble();
        }

        public static int RandomInt(int min, int max)
        {
            return random.Next(min, max);
        }

        public static int RandomInt(int max)
        {
            return RandomInt(0, max);
        }
    }
}
