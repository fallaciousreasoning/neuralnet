﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNet
{
    public static class EnumerableExtensions
    {
        public static void AddRange<T>(this List<T> to, params T[] items)
        {
            to.AddRange(items);
        }

        public static T Min<T>(this List<T> of, Func<T, float> func)
        {
            var minItem = default(T);
            var min = float.MaxValue;

            foreach (var item in of)
            {
                var current = func(item);
                if (current < min)
                {
                    min = current;
                    minItem = item;
                }
            }
            return minItem;
        }

        public static T Max<T>(this List<T> of, Func<T, float> func)
        {
            return of.Min(i => -func(i));
        } 
    }
}
