﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace NeuralNet.Genetics
{
    public class GeneticAlgorithm
    {
        private List<Genome> genomes;

        private int populationSize;
        private int elitism = 2;

        //0.05 to 0.3 work best.
        private readonly float mutationRatio;

        //0.7 works nicely
        private readonly float crossoverRatio;

        private float maxPermutation = 0.05f;

        private float bestFitness;
        private float worstFitness;
        private float totalFitness;
        private float averageFitness;

        public int Generation { get; private set; }

        public GeneticAlgorithm(int populationSize, float mutationRatio, float crossoverRatio)
        {
            this.populationSize = populationSize;
            this.mutationRatio = mutationRatio;
            this.crossoverRatio = crossoverRatio;
        }

        private void CalculateTotals()
        {
            bestFitness = float.MinValue;
            worstFitness = float.MaxValue;
            averageFitness = 0;
            totalFitness = 0;

            foreach (var genome in genomes)
            {
                totalFitness += genome.Fitness;
                if (worstFitness > genome.Fitness)
                    worstFitness = genome.Fitness;
                if (bestFitness < genome.Fitness)
                    bestFitness = genome.Fitness;
            }

            averageFitness = totalFitness/genomes.Count;
        }

        private Genome RouletteSelect()
        {
            var fitnessSlice = RandomUtil.RandomFloat()*totalFitness;
            var fitnessTotal = 0f;

            for (var i = 0; i < genomes.Count; ++i)
            {
                fitnessTotal += genomes[i].Fitness;

                if (fitnessTotal >= fitnessSlice)
                {
                    return genomes[i];
                }
            }

            //Failed to select anything.
            return null;
        }

        private void Mutate(List<float> chromosone)
        {
            for (var i = 0; i < chromosone.Count; ++i)
            {
                if (RandomUtil.RandomFloat() < mutationRatio)
                {
                    chromosone[i] += RandomUtil.RandomClamped() * maxPermutation;
                }
            }
        }

        private void CrossOver(List<float> mother, List<float> father, out List<float> baby1, out List<float> baby2)
        {
            baby1 = new List<float>();
            baby2 = new List<float>();

            if ((RandomUtil.RandomFloat() < crossoverRatio) || (mother == father))
            {
                baby1.AddRange(mother);
                baby2.AddRange(father);
                return;
            }

            var crossOverPoint = RandomUtil.RandomInt(mother.Count);

            for (var i = 0; i < crossOverPoint; ++i)
            {
                baby1.Add(mother[i]);
                baby2.Add(father[i]);
            }

            for (var i = crossOverPoint; i < mother.Count; ++i)
            {
                baby1.Add(father[i]);
                baby2.Add(mother[i]);
            }
        }

        public List<Genome> Epoch(List<Genome> oldPopulation)
        {
            genomes = oldPopulation;
            genomes.Sort((genome, genome1) => (int)MathHelper.Clamp(genome.Fitness - genome1.Fitness, -1, 1));

            CalculateTotals();

            var newPopulation = new List<Genome>();
            for (var i = 0; i < elitism; ++i)
            {
                newPopulation.Add(oldPopulation[oldPopulation.Count - 1 - elitism]);
            }

            while (newPopulation.Count < oldPopulation.Count)
            {
                var mum = RouletteSelect();
                var dad = RouletteSelect();

                List<float> baby1;
                List<float> baby2;

                CrossOver(mum.Weights, dad.Weights, out baby1, out baby2);
                Mutate(baby1);
                Mutate(baby2);

                newPopulation.Add(new Genome(baby1, 0));
                newPopulation.Add(new Genome(baby2, 0));
            }

            Generation ++;
            return newPopulation;
        }
    }
}
