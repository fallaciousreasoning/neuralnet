﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNet.Genetics
{
    public class Genome
    {
        public List<float> Weights { get; private set; }
        public float Fitness { get; private set; }

        public Genome(List<float> weights , float fitness)
        {
            this.Weights = weights;
            this.Fitness = fitness;
        }

        
    }
}
