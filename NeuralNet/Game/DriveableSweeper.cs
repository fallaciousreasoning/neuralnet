﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace NeuralNet.Game
{
    public class DriveableSweeper : Sweeper
    {
        public DriveableSweeper()
        {
            Sprite.Tint = Color.DarkRed;
            Inertia = 50;
        }
        public override void Update(float step)
        {
            var keys = Keyboard.GetState();

            float left = 0, right = 0;

            if (keys.IsKeyDown(Keys.W))
                left++;
            if (keys.IsKeyDown(Keys.S))
                left--;
            if (keys.IsKeyDown(Keys.Up))
                right ++;
            if (keys.IsKeyDown(Keys.Down))
                right--;

            var totalTorque = left - right;

            //Torque = Angular Acceleration * Inertia.
            AngularVelocity = totalTorque / Inertia;

            var totalForce = (left + right) * 0.5f * Transform.FacingDirection;
            //Force = Mass * Acceleration.
            Velocity = totalForce / Mass;

            Transform.Position += Velocity;
            Transform.Rotation += AngularVelocity;

            ScreenLoop();
        }
    }
}
