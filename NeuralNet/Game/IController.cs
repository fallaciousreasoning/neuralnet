﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNet.Game
{
    public interface IController
    {
        void QueueAdd(object o);
        void QueueRemove(object o);
        void Add(object o);
        void Remove(object o);
    }
}
