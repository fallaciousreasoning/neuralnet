﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNet.Game
{
    public interface IEnigmaDrawable
    {
        void Draw();
    }
}
