﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NeuralNet.Game
{
    public class BaseController : IEnigmaUpdatable, IEnigmaDrawable, IController
    {
        private readonly LinkedList<object> toAdd = new LinkedList<object>();
        private readonly LinkedList<object> toRemove = new LinkedList<object>();
         
        private readonly LinkedList<IEnigmaUpdatable> updatables = new LinkedList<IEnigmaUpdatable>(); 
        private readonly LinkedList<IEnigmaDrawable> drawables = new LinkedList<IEnigmaDrawable>();

        public virtual void QueueAdd(object o)
        {
            toAdd.AddLast(o);
        }

        public virtual void QueueRemove(object o)
        {
            toRemove.AddLast(o);
        }
         
        public virtual void Add(object o)
        {
            if (o is IEnigmaUpdatable)
            {
                updatables.AddLast(o as IEnigmaUpdatable);
            }

            if (o is IEnigmaDrawable)
            {
                drawables.AddLast(o as IEnigmaDrawable);
            }

            if (o is IKnowController)
            {
                (o as IKnowController).Controller = this;
            }

            (o as IEnigmaStartable)?.Start();
        }

        public virtual void Remove(object o)
        {
            if (o is IEnigmaUpdatable)
            {
                updatables.Remove(o as IEnigmaUpdatable);
            }

            if (o is IEnigmaDrawable)
            {
                drawables.Remove(o as IEnigmaDrawable);
            }

            (o as IEnigmaDestroyable)?.Destroy();

            if (o is IKnowController)
            {
                (o as IKnowController).Controller = null;
            }
        }

        public virtual void Update(float step)
        {
            foreach (var updatable in updatables) updatable.Update(step);

            foreach (var remove in toRemove)
                Remove(remove);
            foreach (var add in toAdd)
                Add(add);

            toRemove.Clear();
            toAdd.Clear();
        }

        public virtual void Draw()
        {
            foreach (var drawable in drawables)
                drawable.Draw();
        }
    }
}
