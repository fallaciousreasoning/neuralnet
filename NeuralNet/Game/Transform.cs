﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace NeuralNet.Game
{
    public class Transform
    {
        public Vector2 Position { get; set; }
        public Vector2 Scale { get; set; }
        public float Rotation { get; set; }

        public Transform()
        {
            Scale = Vector2.One;
        }

        public Vector2 FacingDirection
        {
            get { return new Vector2((float)Math.Sin(Rotation), -(float)Math.Cos(Rotation)); }
            set
            {
                var newRot = (float) Math.Atan2(value.Y, value.X);
                Rotation = float.IsNaN(newRot) ? 0 : newRot;
            }
        }

        public Vector2 Aim(Vector2 start, Vector2 end)
        {
            return Vector2.Normalize(end - start);
        }

        public Vector2 Aim(Transform start, Transform end)
        {
            return Aim(start.Position, end.Position);
        }
    }
}
