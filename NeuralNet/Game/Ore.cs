﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace NeuralNet.Game
{
    public class Ore : IEnigmaDrawable
    {
        public SweeperGame Game { get; set; }
        public Transform Transform { get; private set; }
        public Sprite Sprite { get; private set; }

        public Ore()
        {
            Transform = new Transform();
            Sprite = new Sprite() { Transform = Transform };
            Sprite.Texture = TextureUtil.CreateTexture(16, 16, Color.Yellow);
        }

        public void Replace()
        {
            var pos = Game.Max;
            pos.X *= RandomUtil.RandomFloat();
            pos.Y *= RandomUtil.RandomFloat();
            Transform.Position = pos;

            if (Vector2.Distance(Game.Max * 0.5f, Transform.Position) < 200)
                Replace();
        }

        public void Draw()
        {
            Sprite.Draw();
        }
    }
}
