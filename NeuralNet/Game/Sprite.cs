﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace NeuralNet.Game
{
    public class Sprite
    {
        private Texture2D texture;

        public Texture2D Texture
        {
            get { return texture; }
            set
            {
                texture = value;
                Origin = new Vector2(Texture.Width, Texture.Height)*0.5f;
            }
        }

        public Vector2 Origin { get; private set; }
        public Color Tint { get; set; }
        public SpriteEffects Effects { get; set; }

        public Transform Transform { get; set; }

        public Sprite()
        {
            Tint = Color.White;
            Effects = SpriteEffects.None;
        }

        public void Draw()
        {
            Game1.SpriteBatch.Draw(Texture, Transform.Position, null, Tint, Transform.Rotation, Origin, Transform.Scale,
                SpriteEffects.None, 0);
        }
    }
}
