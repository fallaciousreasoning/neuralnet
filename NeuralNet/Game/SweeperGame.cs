﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NeuralNet.Genetics;

namespace NeuralNet.Game
{
    public class SweeperGame : BaseController, IEnigmaStartable
    {
        private readonly SpriteFont Arial;
        private readonly List<Sweeper> sweepers = new List<Sweeper>();
        private readonly List<Ore> ores = new List<Ore>();
        private readonly Bad bad = new Bad();
         
        private readonly GeneticAlgorithm geneticAlgorithm;

        public readonly int SweeperCount;
        public readonly int OreCount;
        public readonly int GenerationAt;

        private float timeTillEpoch;

        public SweeperGame(int sweeperCount=10, int oreCount=40, int generationAt=5)
        {
            SweeperCount = sweeperCount;
            OreCount = oreCount;
            GenerationAt = generationAt;
            timeTillEpoch = generationAt*5;

            geneticAlgorithm = new GeneticAlgorithm(SweeperCount, 0.3f, 0.7f);

            Arial = Game1.Content.Load<SpriteFont>("Arial");
        }

        public Vector2 Min { get { return Vector2.Zero; } }
        public Vector2 Max { get { return Vector2.Transform(new Vector2(Game1.GameWidth, Game1.GameHeight), Matrix.Invert(view)); } }
        private Matrix view = Matrix.Identity * Matrix.CreateScale(0.5f);

        public override void Add(object o)
        {
            var sweeper = o as Sweeper;
            if (sweeper != null)
            {
                sweepers.Add(sweeper);
                sweeper.Game = this;
            }

            var ore = o as Ore;
            if (ore != null)
            {
                ores.Add(ore);
            }

            base.Add(o);
        }

        public override void Remove(object o)
        {
            var sweeper = o as Sweeper;
            if (sweeper != null)
            {
                sweepers.Remove(sweeper);
                sweeper.Game = null;
            }

            var ore = o as Ore;
            if (ore != null)
            {
                ores.Remove(ore);
            }

            base.Remove(o);
        }

        public object ClosestOre(Sweeper to)
        {
            var ore = ores.Min(o => Vector2.Distance(to.Transform.Position, o.Transform.Position));
            return ore;
            //if (Vector2.Distance(ore.Transform.Position, to.Transform.Position) <
            //    Vector2.Distance(bad.Transform.Position, to.Transform.Position) - 160)
            //    return ore;
            //return bad;
        }

        public int Best
        {
            get { return sweepers.Max(s => s.Collected).Collected; }
        }

        public void Start()
        {
            for (var i = 0; i < SweeperCount; ++i)
            {
                var sweeper = new Sweeper();
                sweeper.Transform.Position = Max*RandomUtil.RandomFloat();
                sweeper.Brain = new Brain.NeuralNet(4, 2, 1, 6);

                Add(sweeper);
            }

            bad.Transform.Position = Max*0.5f;
            //Add(bad);
            //Add(new DriveableSweeper() {Brain = new Brain.NeuralNet(4, 2, 2, 6)});

            for (var i = 0; i < OreCount; ++i)
            {
                var ore = new Ore();
                ore.Game = this;
                ore.Replace();

                Add(ore);
            }
        }

        public override void Update(float step)
        {
            step *= 5;
            timeTillEpoch -= step;
            base.Update(step);

            if (sweepers.Any(s => s.Collected >= (GenerationAt * 10) + 5) || timeTillEpoch <= 0)
            {
                var genomes = (from sweeper in sweepers select sweeper.Genome).ToList();
                genomes = geneticAlgorithm.Epoch(genomes);

                for (var i = 0; i < genomes.Count; ++i)
                {
                    sweepers[i].Genome = genomes[i];
                }
                timeTillEpoch = GenerationAt*5;
            }
        }

        public override void Draw()
        {
            Game1.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, view);
            base.Draw();
            Game1.SpriteBatch.End();

            Game1.SpriteBatch.Begin();
            
            Game1.SpriteBatch.DrawString(Arial, $"Best: {Best}", new Vector2(20, 20), Color.Black);
            Game1.SpriteBatch.DrawString(Arial, $"Generation {geneticAlgorithm.Generation}", new Vector2(20, 40), Color.Black);
            Game1.SpriteBatch.DrawString(Arial, $"Time Till Epoch {timeTillEpoch}", new Vector2(20, 60), Color.Black);

            Game1.SpriteBatch.End();
        }
    }
}
