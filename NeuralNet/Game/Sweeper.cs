﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NeuralNet.Genetics;

namespace NeuralNet.Game
{
    public class Sweeper : IEnigmaUpdatable, IEnigmaDrawable
    {
        public SweeperGame Game { get; set; }

        public int Collected { get; set; }

        public Vector2 Velocity { get; protected set; }
        public float AngularVelocity { get; protected set; }

        public Transform Transform { get; private set; }
        public Sprite Sprite { get; private set; }
        public Brain.NeuralNet Brain { get; set; }

        public float TrackRadius { get; set; }
        public float TrackForce { get; set; }

        public float Inertia { get; set; }
        public float Mass { get; set; }
        public float MaxTurnRate { get; set; }

        private static Texture2D pointer = TextureUtil.CreateTexture(2, 32, Color.White);

        private Vector2 facingDir = new Vector2(0, -1);
        private Vector2 coinDir = new Vector2(0, -1);
        private float leftTrack = 0f;
        private float rightTrack = 0f;

        public Sweeper()
        {
            Transform = new Transform();

            Sprite = new Sprite();
            Sprite.Transform = Transform;
            Sprite.Texture = TextureUtil.CreateTexture(32, 32, Color.White);

            TrackRadius = 1;
            Inertia = 1;
            TrackForce = 60;
            Mass = 1;
            MaxTurnRate = 0.3f;
            Collected = 5;
        }

        public virtual void Update(float step)
        {
            var closest = Game.ClosestOre(this);
            var ore = closest as Ore;
            var bad = closest as Bad;
            var transform = ore?.Transform ?? bad.Transform;
            var dir = Transform.Aim(Transform, transform);
            facingDir = Transform.FacingDirection;
            coinDir = dir;
            var input = new List<float>(5) {Transform.FacingDirection.X, Transform.FacingDirection.X, dir.X, dir.Y};
            var result = Brain.OutputFor(input);

            float left = result[0] * TrackRadius * TrackForce, right = result[1] * TrackRadius * TrackForce;
            var totalTorque = left - right;
            leftTrack = left;
            rightTrack = right;

            //Torque = Angular Acceleration * Inertia.
            AngularVelocity = totalTorque/Inertia;

            var totalForce = (left + right)*Transform.FacingDirection;
            //Force = Mass * Acceleration.
            Velocity = totalForce/Mass;

            Transform.Position += Velocity * step;
            Transform.Rotation += MathHelper.Clamp(AngularVelocity * step, -MaxTurnRate, MaxTurnRate);

            var dist = Vector2.Distance(transform.Position, Transform.Position);
            if (dist < 32 && ore != null)
            {
                Collected += 10;
                ore.Replace();
            }

            if (dist < 128 && bad != null)
            {
                Collected = 0;
            }

            ScreenLoop();
        }

        protected void ScreenLoop()
        {
            var newPos = Transform.Position;
            if (Transform.Position.X < 0)
                newPos.X = Game.Max.X;

            if (Transform.Position.Y < 0)
                newPos.Y = Game.Max.Y;

            if (Transform.Position.X > Game.Max.X)
                newPos.X = 0;

            if (Transform.Position.Y > Game.Max.Y)
                newPos.Y = 0;

            Transform.Position = newPos;
        }

        public void Draw()
        {
            Sprite.Tint = Collected == Game.Best ? Color.Red : Color.White;
            Sprite.Draw();

            var bottom = new Vector2(pointer.Width * 0.5f, pointer.Height);
            var coinRot = (float)Math.Atan2(coinDir.X, -coinDir.Y);
            Game1.SpriteBatch.Draw(pointer, Transform.Position, null, Color.Red, coinRot, bottom, 1f, SpriteEffects.None, 0f);

            Game1.SpriteBatch.Draw(pointer, Transform.Position, null, Color.Green, Transform.Rotation, bottom, 1f, SpriteEffects.None, 0f);

            var halfWidth = Sprite.Texture.Width * 0.5f;
            var halfHeight = Sprite.Texture.Height*0.5f;
            var max = TrackForce*TrackRadius;

            Game1.SpriteBatch.Draw(pointer, Transform.Position + Vector2.Transform(new Vector2(-halfWidth, -halfHeight), Matrix.CreateRotationZ(Transform.Rotation)), null, Color.Blue, Transform.Rotation, bottom, new Vector2(1, leftTrack/max), SpriteEffects.None, 0f);
            Game1.SpriteBatch.Draw(pointer, Transform.Position + Vector2.Transform(new Vector2(halfWidth, -halfHeight), Matrix.CreateRotationZ(Transform.Rotation)), null, Color.Blue, Transform.Rotation, bottom, new Vector2(1, rightTrack / max), SpriteEffects.None, 0f);

        }

        public Genome Genome
        {
            get { return new Genome(Brain.GetWeights(), Collected); }
            set
            {
                Brain.SetWeights(value.Weights);
                Collected = 5;
            }
        }
    }
}
