﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace NeuralNet.Game
{
    public class Bad : IEnigmaDrawable
    {
        public Transform Transform { get; set; }
        public Sprite Sprite { get; set; }

        public Bad()
        {
            Transform = new Transform();
            Sprite = new Sprite() {Transform =  Transform, Texture = TextureUtil.CreateTexture(128, 128, Color.Green)};
        }

        public void Draw()
        {
            Sprite.Draw();
        }
    }
}
