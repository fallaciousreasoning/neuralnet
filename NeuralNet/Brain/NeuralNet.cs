﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNet.Brain
{
    public class NeuralNet
    {
        private int inputNodes;
        private NeuronLayer outputNodes;

        private List<NeuronLayer> layers;

        public NeuralNet(int inputs, int outputs, int hiddenLayers, int nodesPerHiddenLayer)
        {
            this.inputNodes = inputs;
            InitializeNet(inputs, outputs, hiddenLayers, nodesPerHiddenLayer);
        }

        private void InitializeNet(int inputs, int outputs, int hiddenLayers, int nodesPerHiddenLayer)
        {
            layers = new List<NeuronLayer>();
            outputNodes = new NeuronLayer(outputs, nodesPerHiddenLayer);

            for (var i = 0; i < hiddenLayers; ++i)
            {
                var inputsCount = i == 0 ? inputs : nodesPerHiddenLayer;
                layers.Add(new NeuronLayer(nodesPerHiddenLayer, inputsCount));
            }

            layers.Add(outputNodes);
        }

        public List<float> OutputFor(List<float> inputs)
        {
            foreach (var layer in layers)
            {
                if (inputs.Count != layer.InputCount) throw new ArgumentException("Invalid number of inputs or poorly constructed net!");
                inputs = layer.CalculateOutputs(inputs);
            }

            return inputs;
        }

        public static float Sigmoid(float value, float activationResponse)
        {
            return (float)(1 / (1 + Math.Exp(-value / activationResponse)));
        }

        public List<float> GetWeights()
        {
            var weights = new List<float>();
            foreach (var layer in layers)
            {
                foreach (var neuron in layer.Neurons)
                {
                    weights.AddRange(neuron.Weightings);
                }
            }
            return weights;
        }

        public void SetWeights(List<float> weights)
        {
            var i = 0;
            foreach (var layer in layers)
            {
                foreach (var neuron in layer.Neurons)
                {
                    for (var j = 0; j < layer.InputCount; ++j)
                    {
                        neuron.Weightings[j] = weights[i + j];
                    }
                    i += layer.InputCount;
                }
            }
        }
    }
}
