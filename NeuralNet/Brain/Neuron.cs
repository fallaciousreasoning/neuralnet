﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace NeuralNet.Brain
{
    public class Neuron
    {
        public List<float> Weightings { get; set; }
        public float ActivationThreshold { get; set; }

        public Neuron(int inputCount)
        {
            Weightings = new List<float>(inputCount);
            for (var i = 0; i < inputCount; ++i)
                Weightings.Add(RandomUtil.RandomClamped());
        }

        public float CalculateOutput(List<float> inputs)
        {
            var total = inputs.Select((input, i) => input*Weightings[i]).Sum() + ActivationThreshold*-1;
            return NeuralNet.Sigmoid(total, 1f);
        }
    }
}
