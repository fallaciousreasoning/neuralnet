﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuralNet.Brain
{
    public class NeuronLayer
    {
        public List<Neuron> Neurons { get; set; }

        public readonly int InputCount;

        public NeuronLayer(int neuronCount, int inputs)
        {
            this.InputCount = inputs;
            Neurons = new List<Neuron>(neuronCount);

            for (var i = 0; i < neuronCount; ++i)
            {
                Neurons.Add(new Neuron(inputs));
            }
        }

        public List<float> CalculateOutputs(List<float> inputs)
        {
            if (inputs.Count != InputCount) throw new ArgumentException("Invalid number of inputs!");

            var outputs = new List<float>(Neurons.Count);
            outputs.AddRange(Neurons.Select(neuron => neuron.CalculateOutput(inputs)));
            return outputs;
        }
    }
}
